<?php

/**
 * @package   yii2-image-resizer
 * @author    Nur Wahyudin <wahyu1790@gmail.com>
 * @copyright Copyright &copy; Nur Wahyudin, nurwahyudin.com, 2015
 * @version   1.0
 */

namespace permasoft\yii2\image;

use Yii;
use yii\base\Component;
use yii\base\InvalidConfigException;
use yii\base\InvalidParamException;
use yii\helpers\FileHelper;
use yii\helpers\Inflector;
use yii\helpers\Url;
use Eventviva\ImageResize;

/**
 * @author Nur Wahyudin <wahyu1790@gmail.com>
 * @since 1.0
 */
class ImageResizer extends Component
{
    /**
     * Original image file folder. Can be either alias, absolute, or relative path.
     * @var string|array
     */
    public $imageSourcePath;

    /**
     * Folder for storing resized images.. Can be either alias, absolute, or relative path.
     * @var string|array
     */
    public $imageCachePath;

    /**
     * Image placeholder to be used when product doesn't have an image or the image aren't exist.
     * Can be either alias, absolute, or relative path.
     * @var string
     */
    public $imagePlaceholder;

    /**
     * Path to the image that would be resized (original image).
     * @var string
     */
    private $originalImage;

    /**
     * Original image name.
     * @var string
     */
    private $originalImageName;

    /**
     * Original image width.
     * @var integer
     */
    private $originalImageWidth;

    /**
     * Original image height.
     * @var integer
     */
    private $originalImageHeight;

    /**
     * Path to the original image.
     * @var string
     */
    private $originalImagePath;

    /**
     * URL to the original image.
     * @var string
     */
    private $originalImageURL;

    /**
     * The resized image.
     * @var image
     */
    private $resizedImage;

    /**
     * Resized image name.
     * @var string
     */
    private $resizedImageName;

    /**
     * Optimal width for the resized image.
     * @var integer
     */
    private $resizedImageWidth;

    /**
     * Optimal height for the resized image.
     * @var integer
     */
    private $resizedImageHeight;

    /**
     * Path to the resized image.
     * @var string
     */
    private $resizedImagePath;

    /**
     * URL to the resized image.
     * @var string
     */
    private $resizedImageURL;

    /**
     * Image mime type.
     * @var string
     */
    private $mimeType;

    /**
     * Image category.
     * @var string
     */
    private $category;

    /**
     * Get the original image name.
     * @return string
     */
    public function getOriginalImageName()
    {
        $image = explode(DIRECTORY_SEPARATOR, $this->getOriginalImagePath());
        $this->originalImageName = $image[count($image)-1];
        return $this->originalImageName;
    }

    /**
     * Get path to the original image.
     * @return string
     */
    public function getOriginalImagePath()
    {
        return $this->originalImagePath;
    }

    /**
     * Get URL for viewing the original image.
     * @return string
     */
    public function getOriginalImageURL()
    {
        return Url::to('/images/' . Inflector::singularize($this->category) . '/' . $this->getOriginalImageName());
    }

    /**
     * Get resized image name.
     * @return string
     */
    public function getResizedImageName()
    {
        return $this->resizedImageName;
    }

    /**
     * Get path to the resized image.
     * @return string
     */
    public function getResizedImagePath()
    {
        return $this->resizedImagePath;
    }

    /**
     * Get URL for viewing the resized image.
     * @return string
     */
    public function getResizedImageURL()
    {
        $url = $this->getOriginalImageURL();
        $sizedURL = !empty($this->resizedImageWidth) && !empty($this->resizedImageHeight) ? '?size='.$this->resizedImageWidth.'x'.$this->resizedImageHeight : null;
        return Url::to($url . $sizedURL);
    }

    /**
     * Prepare the image for resizing.
     * @param  string $image Path to the image file. Can use path alias.
     * @return this
     */
    public function load($image, $category = 'uncategorized')
    {
        $this->category = Inflector::pluralize($category);
        $this->originalImagePath = $image;
        $this->originalImagePath = $this->findImage();
        $this->mimeType = FileHelper::getMimeType($this->originalImagePath);

        switch ($this->mimeType) {
            case 'image/jpeg':
                $image = @imagecreatefromjpeg($this->originalImagePath);
                break;

            case 'image/png':
                $image = @imagecreatefrompng($this->originalImagePath);
                break;

            default:
                throw new \Exception("Unsupported image type.");
                break;
        }

        $this->image = $image;
        $this->imageName = $this->getImageName();
        $this->imageWidth = imagesx($this->image);
        $this->imageHeight = imagesy($this->image);

        return $this;
    }
}